package com.skeedtech.minimalp2p.client;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.skeedtech.minimalp2p.common.SomeRequest;
import com.skeedtech.minimalp2p.common.SomeResponse;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created with IntelliJ IDEA.
 * User: Nakamura
 * Date: 13/05/17
 * Time: 16:06
 */
public class SendClient {


	public static void main(String[] args) throws IOException {
		final AtomicBoolean someThing = new AtomicBoolean(true);
		Client client = new Client();
		Kryo kryo = client.getKryo();
		kryo.register(SomeRequest.class);
		kryo.register(SomeResponse.class);

		final Object lock = new Object();

		client.start();
		client.connect(5000, "192.168.111.14", 54555, 54777);

		SomeRequest request = new SomeRequest();
		request.text = "Here is the request!";

		client.addListener(new Listener() {
			public void received (Connection connection, Object object) {
				if (object instanceof SomeResponse) {
					SomeResponse response = (SomeResponse)object;
					System.out.println(response.text);
					someThing.set(false);
				}
			}
		});
		client.sendTCP(request);

		while(someThing.get()) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
