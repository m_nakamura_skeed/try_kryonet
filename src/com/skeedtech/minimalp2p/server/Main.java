package com.skeedtech.minimalp2p.server;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

import com.skeedtech.minimalp2p.common.SomeRequest;
import com.skeedtech.minimalp2p.common.SomeResponse;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

public class Main {

	final private static AtomicReference<Server> serverRef = new AtomicReference<Server>(null);

	public static void main(String[] args) throws IOException {

		Runtime.getRuntime().addShutdownHook(
				new Thread() {
					public void run() {
						System.out.println("Call ShutdownHook");
						// 終了処理
						shutdown();
					}
				}
		);

		startup();
	}

	private static void startup() throws IOException {
		// ******************************
		// 起動
		// ******************************
		final Server server = new Server();
		final Kryo kryo = server.getKryo();
		kryo.register(SomeRequest.class);
		kryo.register(SomeResponse.class);

		server.start();
		server.bind(54555, 54777);

		server.addListener(new Listener() {
			public void received (Connection connection, Object object) {
				if (object instanceof SomeRequest) {
					SomeRequest request = (SomeRequest)object;
					System.out.println(request.text);

					SomeResponse response = new SomeResponse();
					response.text = "Thanks!";
					connection.sendTCP(response);
				}
			}
		});

		serverRef.set(server);
	}

	private static void shutdown() {
		if(serverRef.get() != null) {
			final Server server = serverRef.get();
			server.stop();
		}
	}
}
